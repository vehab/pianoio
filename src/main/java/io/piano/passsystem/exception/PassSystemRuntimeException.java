package io.piano.passsystem.exception;

public class PassSystemRuntimeException
        extends RuntimeException {

    public PassSystemRuntimeException(String message) {
        super(message);
    }

    public PassSystemRuntimeException(String message,
                                      Throwable cause) {
        super(message, cause);
    }

}

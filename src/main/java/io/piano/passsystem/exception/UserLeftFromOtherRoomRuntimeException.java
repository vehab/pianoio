package io.piano.passsystem.exception;

public class UserLeftFromOtherRoomRuntimeException
        extends PassSystemActionRuntimeException {

    private final int currentRoomId;
    private final int nextRoomId;
    private final long userId;

    public UserLeftFromOtherRoomRuntimeException(boolean isAction,
                                                 int currentRoomId,
                                                 int nextRoomId,
                                                 long userId) {
        super(isAction, String.format("выход из комнаты, но уже в другой комнате : текущий roomId='%1$s', следующий roomId='%2$s', userId='%3$s'",
                currentRoomId, nextRoomId, userId));

        this.currentRoomId = currentRoomId;
        this.nextRoomId = nextRoomId;
        this.userId = userId;
    }

    public int getCurrentRoomId() {
        return currentRoomId;
    }

    public int getNextRoomId() {
        return nextRoomId;
    }

    public long getUserId() {
        return userId;
    }

}

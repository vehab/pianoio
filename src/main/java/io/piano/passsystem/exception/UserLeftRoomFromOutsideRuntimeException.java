package io.piano.passsystem.exception;

public class UserLeftRoomFromOutsideRuntimeException
        extends PassSystemActionRuntimeException {

    private final int roomId;
    private final long userId;

    public UserLeftRoomFromOutsideRuntimeException(boolean isAction,
                                                   int roomId,
                                                   long userId) {
        super(isAction, String.format("выход из комнаты, но уже снаружи : roomId='%1$s', userId='%2$s'", roomId, userId));

        this.roomId = roomId;
        this.userId = userId;
    }

    public int getRoomId() {
        return roomId;
    }

    public long getUserId() {
        return userId;
    }

}

package io.piano.passsystem.exception;

public class PassSystemActionRuntimeException
        extends PassSystemRuntimeException {

    public PassSystemActionRuntimeException(boolean isAction,
                                            String message) {
        super("Запрещенное " + (isAction ? "действие" : "намерение") + " : " + message);
    }

}

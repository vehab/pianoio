package io.piano.passsystem.exception;

public class UserRepeatEnteredRoomRuntimeException
        extends PassSystemActionRuntimeException {

    private final int roomId;
    private final long userId;

    public UserRepeatEnteredRoomRuntimeException(boolean isAction,
                                                 int roomId,
                                                 long userId) {
        super(isAction, String.format("вход в комнату, но уже в этой комнате : roomId='%1$s', userId='%2$s'", roomId, userId));

        this.roomId = roomId;
        this.userId = userId;
    }

    public int getRoomId() {
        return roomId;
    }

    public long getUserId() {
        return userId;
    }

}

package io.piano.passsystem.exception;

public class InvalidRoomIdRuntimeException
        extends PassSystemRuntimeException {

    private final long roomId;

    public InvalidRoomIdRuntimeException(long roomId) {
        super("Invalid room id '" + roomId + "'!");

        this.roomId = roomId;
    }

    public long getRoomd() {
        return roomId;
    }

}

package io.piano.passsystem.exception;

public class InvalidUserIdRuntimeException
        extends PassSystemRuntimeException {

    private final long userId;

    public InvalidUserIdRuntimeException(long userId) {
        super("Invalid user id '" + userId + "'!");

        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

}

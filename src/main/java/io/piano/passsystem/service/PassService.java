package io.piano.passsystem.service;

import io.piano.passsystem.exception.InvalidRoomIdRuntimeException;
import io.piano.passsystem.exception.InvalidUserIdRuntimeException;
import io.piano.passsystem.exception.PassSystemRuntimeException;
import io.piano.passsystem.exception.RoomAccessDeniedRuntimeException;
import io.piano.passsystem.exception.UserDoubleEnteredRoomRuntimeException;
import io.piano.passsystem.exception.UserLeftFromOtherRoomRuntimeException;
import io.piano.passsystem.exception.UserLeftRoomFromOutsideRuntimeException;
import io.piano.passsystem.exception.UserRepeatEnteredRoomRuntimeException;
import io.piano.passsystem.model.EntranceType;
import io.piano.passsystem.model.UserRoomState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class PassService {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final ConcurrentMap<Long, UserRoomState> usersAndRooms = new ConcurrentHashMap<>();

    public void move(EntranceType entranceType,
                     int roomId,
                     long userId) {
        final boolean isAction = true;

        validateUserAndRoom(isAction, roomId, userId);

        UserRoomState userRoomState = entranceType == EntranceType.out ? UserRoomState.OUT : new UserRoomState(roomId);
        usersAndRooms.putIfAbsent(userId, userRoomState);
        usersAndRooms.merge(userId, userRoomState,
                (currentUserRoomState, nextUserRoomState) -> {
                    Integer currentRoomId = currentUserRoomState.getRoomId().orElse(null);

                    try {
                        processUserAction(isAction, entranceType, roomId, userId, currentRoomId);
                    } catch (PassSystemRuntimeException ex) {
                        log(isAction, userId, roomId, ex.getMessage());
                        //throw ex;
                    }

                    return nextUserRoomState;
                });
    }

    public String check(EntranceType entranceType,
                        int roomId,
                        long userId) {
        final boolean isAction = false;
        try {
            validateUserAndRoom(isAction, roomId, userId);

            UserRoomState userRoomState = usersAndRooms.get(userId);
            Integer currentRoomId = userRoomState == null ? null : userRoomState.getRoomId().orElse(null);

            processUserAction(isAction, entranceType, roomId, userId, currentRoomId);

            return "Доступ разрешен";
        } catch (PassSystemRuntimeException ex) {
            log(isAction, userId, roomId, ex.getMessage());
            throw ex;
        }
    }

    private void validateUserAndRoom(boolean isAction,
                                     int roomId,
                                     long userId) {
        if (1 > roomId || roomId > 5) {
            throw new InvalidRoomIdRuntimeException(roomId);
        }
        if (1 > userId || userId > 10_000) {
            throw new InvalidUserIdRuntimeException(userId);
        }

        if (userId % roomId != 0) {
            throw new RoomAccessDeniedRuntimeException(isAction, roomId, userId);
        }
    }

    /**
     * @param isAction - намерение (false) или действие (true)
     */
    private void processUserAction(boolean isAction,
                                   EntranceType entranceType,
                                   int roomId,
                                   long userId,
                                   Integer currentRoomId) {
        if (currentRoomId == null) {
            if (entranceType == EntranceType.out) {
                throw new UserLeftRoomFromOutsideRuntimeException(isAction, roomId, userId);
            } else {
                String isActionMessage = "Разрешенное " + (isAction ? "действие" : "намерение");
                log.info("userId={}, roomId={}; {} : вход", userId, roomId, isActionMessage);
            }
        } else if (currentRoomId.equals(roomId)) {
            if (entranceType == EntranceType.out) {
                String isActionMessage = "Разрешенное " + (isAction ? "действие" : "намерение");
                log.info("userId={}, roomId={}; {} : выход", userId, roomId, isActionMessage);
            } else {
                throw new UserRepeatEnteredRoomRuntimeException(isAction, roomId, userId);
            }
        } else {
            if (entranceType == EntranceType.out) {
                throw new UserLeftFromOtherRoomRuntimeException(isAction, currentRoomId, roomId, userId);
            } else {
                throw new UserDoubleEnteredRoomRuntimeException(isAction, currentRoomId, roomId, userId);
            }
        }
    }

    private void log(boolean isAction,
                     long userId,
                     int roomId,
                     String message) {
        String isActionMessage = "Запрещенное " + (isAction ? "действие" : "намерение");
        log.info("userId={}, roomId={}; {} : {}", userId, roomId, isActionMessage, message);
    }

}

package io.piano.passsystem.web;

import io.piano.passsystem.model.EntranceType;
import io.piano.passsystem.service.PassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class PassRestController {

    @Autowired
    private PassService service;

    @GetMapping("/check")
    public String check(@RequestParam int roomId,
                        @RequestParam boolean entrance,
                        @RequestParam("keyId") long userId) {
        return service.check(EntranceType.of(entrance), roomId, userId);
    }

    @GetMapping("/move")
    public void move(@RequestParam int roomId,
                     @RequestParam boolean entrance,
                     @RequestParam("keyId") long userId) {
        service.move(EntranceType.of(entrance), roomId, userId);
    }

}

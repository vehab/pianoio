package io.piano.passsystem.web;

import io.piano.passsystem.exception.PassSystemRuntimeException;
import io.piano.passsystem.exception.RoomAccessDeniedRuntimeException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ResponseExceptionHandler {

    @Order(Integer.MIN_VALUE)
    @ExceptionHandler(PassSystemRuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handle500(PassSystemRuntimeException ex) {
        return ex.toString();
    }

    @Order(1)
    @ExceptionHandler(RoomAccessDeniedRuntimeException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handle403(RoomAccessDeniedRuntimeException ex) {
        return ex.toString();
    }

}

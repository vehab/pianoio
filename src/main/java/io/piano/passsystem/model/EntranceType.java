package io.piano.passsystem.model;

public enum EntranceType {

    in,
    out;

    public static EntranceType of(boolean entrance) {
        return entrance ? in : out;
    }

}

package io.piano.passsystem.model;

import java.util.Objects;
import java.util.Optional;

public class UserRoomState {

    public static final UserRoomState OUT = new UserRoomState(null);

    private Optional<Integer> roomId;

    public UserRoomState(Integer roomId) {
        this.roomId = Optional.ofNullable(roomId);
    }

    public Optional<Integer> getRoomId() {
        return roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoomState that = (UserRoomState) o;
        return Objects.equals(roomId, that.roomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId.orElse(null));
    }

    @Override
    public String toString() {
        return String.format("UserRoomState{roomId=%1$s}", roomId.map(Object::toString).orElse("outside"));
    }

}

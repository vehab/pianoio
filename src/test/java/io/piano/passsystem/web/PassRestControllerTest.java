package io.piano.passsystem.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PassRestControllerTest
        extends AbstractTestNGSpringContextTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test(priority = 1, description = "method : check / http.response.code=200")
    public void checkHttpResponseCode200() {
        ResponseEntity<String> response =
                restTemplate.getForEntity(getFullUrl("/check?roomId=1&entrance=true&keyId=1"), String.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test(priority = 1, description = "method : check / http.response.code=403")
    public void checkHttpResponseCode403() {
        ResponseEntity<String> response =
                restTemplate.getForEntity(getFullUrl("/check?roomId=2&entrance=true&keyId=1"), String.class);

        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    @Test(priority = 1, description = "method : check / http.response.code=500")
    public void checkHttpResponseCode500() {
        ResponseEntity<String> response =
                restTemplate.getForEntity(getFullUrl("/check?roomId=6&entrance=true&keyId=1"), String.class);

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    private String getFullUrl(String path) {
        return "http://localhost:" + port + path;
    }

}

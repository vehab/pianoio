package io.piano.passsystem.service;

import io.piano.passsystem.exception.InvalidRoomIdRuntimeException;
import io.piano.passsystem.exception.InvalidUserIdRuntimeException;
import io.piano.passsystem.exception.RoomAccessDeniedRuntimeException;
import io.piano.passsystem.exception.UserDoubleEnteredRoomRuntimeException;
import io.piano.passsystem.exception.UserLeftFromOtherRoomRuntimeException;
import io.piano.passsystem.exception.UserLeftRoomFromOutsideRuntimeException;
import io.piano.passsystem.exception.UserRepeatEnteredRoomRuntimeException;
import io.piano.passsystem.model.EntranceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@SpringBootTest
public class PassServiceTest
        extends AbstractTestNGSpringContextTests {

    @Autowired
    private PassService passService;

    @Test(priority = 1,
            description = "method : check / InvalidRoomIdRuntimeException",
            dataProvider = "dataProviderCheckInvalidRoomIdRuntimeException",
            expectedExceptions = InvalidRoomIdRuntimeException.class)
    public void checkInvalidRoomIdRuntimeException(int roomId) {
        passService.check(EntranceType.in, roomId, 1);
    }

    @DataProvider(name = "dataProviderCheckInvalidRoomIdRuntimeException")
    public static Object[][] dataProviderCheckInvalidRoomIdRuntimeException() {
        return new Object[][]{
                {-1}, {0}, {6}, {7},
        };
    }

    @Test(priority = 2,
            description = "method : check / InvalidUserIdRuntimeException",
            dataProvider = "dataProviderCheckInvalidUserIdRuntimeException",
            expectedExceptions = InvalidUserIdRuntimeException.class)
    public void checkInvalidUserIdRuntimeException(long userId) {
        passService.check(EntranceType.in, 1, userId);
    }

    @DataProvider(name = "dataProviderCheckInvalidUserIdRuntimeException")
    public static Object[][] dataProviderCheckInvalidUserIdRuntimeException() {
        return new Object[][]{
                {-1}, {0}, {10_001}, {10_002},
        };
    }

    @Test(priority = 3,
            description = "method : check / RoomAccessDeniedRuntimeException",
            dataProvider = "dataProviderCheckRoomAccessDeniedRuntimeException",
            expectedExceptions = RoomAccessDeniedRuntimeException.class)
    public void checkRoomAccessDeniedRuntimeException(int roomId,
                                                      long userId) {
        passService.check(EntranceType.in, roomId, userId);
    }

    @DataProvider(name = "dataProviderCheckRoomAccessDeniedRuntimeException")
    public static Object[][] dataProviderCheckRoomAccessDeniedRuntimeException() {
        return new Object[][]{
                {2, 3}, {3, 4}, {4, 5}, {5, 6}, {5, 6},
        };
    }

    @Test(priority = 4,
            description = "method : check / UserLeftRoomFromOutsideRuntimeException",
            expectedExceptions = UserLeftRoomFromOutsideRuntimeException.class)
    public void checkUserLeftRoomFromOutsideRuntimeException() {
        passService.move(EntranceType.in, 1, 1);
        passService.move(EntranceType.out, 1, 1);

        passService.check(EntranceType.out, 1, 1);
    }

    @Test(priority = 5,
            description = "method : check / UserRepeatEnteredRoomRuntimeException",
            expectedExceptions = UserRepeatEnteredRoomRuntimeException.class)
    public void checkUserRepeatEnteredRoomRuntimeException() {
        passService.move(EntranceType.in, 1, 1);

        passService.check(EntranceType.in, 1, 1);
    }

    @Test(priority = 6,
            description = "method : check / UserLeftFromOtherRoomRuntimeException",
            expectedExceptions = UserLeftFromOtherRoomRuntimeException.class)
    public void checkUserLeftFromOtherRoomRuntimeException() {
        passService.move(EntranceType.in, 1, 4);

        passService.check(EntranceType.out, 2, 4);
    }

    @Test(priority = 6,
            description = "method : check / UserDoubleEnteredRoomRuntimeException",
            expectedExceptions = UserDoubleEnteredRoomRuntimeException.class)
    public void checkUserDoubleEnteredRoomRuntimeException() {
        passService.move(EntranceType.in, 1, 4);

        passService.check(EntranceType.in, 2, 4);
    }

}
